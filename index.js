//5
const http = require("http");
//6
const port = 3000;
//7
const server = http.createServer(function(req,res){
	//9
	if(req.url =="/login"){
	res.writeHead(200, {'Content-Type': 'text/plain'});

	res.end("Welcome to the login page");

	}
	//11
	else {
	res.writeHead(404, {'Content-Type': 'text/plain'});

	res.end("Sorry the page you are looking for cannot be found");
	}
});
//7
server.listen(port);
//8
console.log(`Server is up now at localhost:${port}`);