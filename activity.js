/*

1. What directive is used by Node.js in loading the modules it needs?

	answer: require()

2. What Node.js module contains a method for server creation?

	answer: http module
3. What is the method of the http object responsible for creating a server using Node.js?

	answer: createServer()

4. What method of the response object allows us to set status codes and content types?

	answer: writeHead()

5. Where will console.log() output its contents when run in Node.js?

	answer: terminal/git bash

6. What property of the request object contains the address's endpoint?

	answer: req.url()

*/